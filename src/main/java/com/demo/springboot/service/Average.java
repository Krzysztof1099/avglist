package com.demo.springboot.service;

import com.demo.springboot.dto.AverageDto;

import java.util.List;

public interface Average {
    AverageDto Calculate(List<Double> digits);

}
