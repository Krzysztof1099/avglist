package com.demo.springboot.rest;

import com.demo.springboot.dto.AverageDto;
import com.demo.springboot.service.Average;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/")
public class DocumentApiController {

    private final Average average;

    public DocumentApiController(Average average) {
        this.average = average;
    }

    @GetMapping("avg")
        public ResponseEntity<AverageDto> averageCalculate(@RequestParam("digits") List<Double> digits){
            return new ResponseEntity<>(average.Calculate(digits), HttpStatus.OK);
    }
}
